There are three installers to choose from:

DeskSyncNetSetup.msi          - Silent MSI installer
DeskSyncNetSetup-msiexec.msi  - MSI installer suitable for use with msiexec
DeskSyncNetSetup.exe          - Regular installer

The installer must be run with adminstrator rights.